import test_script
def test_one():
    file = 'Data_Sample.txt'
    result = test_script.test(file)
    print(result)
    assert result == {('202', 'samples 3'): 'Plural Error', ('202', 'description'): 'Not Unique', ('202', 'sample       2'): ', Spacing Error'}

def test_plural_output():
    md_value = 'Data Isuessssss'
    result = test_script.rule_for_checking_plural(md_value)
    print(result)
    assert result == "Plural Error"

def test_whitespace_in_middle__output():
    md_value = 'Data   Issue'
    result = test_script.rule_for_checking_white_spaces(md_value)
    print(result)
    assert result == "Spacing Error"

def test_whitespace_at_starting_output():
    md_value = '  Data Issue'
    result = test_script.rule_for_checking_white_spaces(md_value)
    print(result)
    assert result == "Error at starting"

def test_uniqueness_output():
    md_name = 'ID 1'
    chip_id = '200'
    chip_type = '1'
    ds_id = '123456'
    uniqueness_result = [('1', '200', 'ID 1', '123456')]
    result = test_script.rule_for_checking_uniqueness(chip_type,chip_id,md_name,ds_id,uniqueness_result)
    print(result)
    assert result == "Not Unique"

def count_same__md_name_test():
    md_name_list = ['ID', 'ID', 'sample', 'description']
    result = test_script.common_number_of_md_name(md_name_list)
    print(result)
    assert result == "Discrepancy found. Expected list should be as MD NAME : ID and COUNT : 2 Error Details are : Counter({'ID': 2, 'description': 1, 'sample': 1})"

def count_unique_md_name_test():
    md_name_list = ['ID','ID','sample','sample', 'description', 'description']
    result = test_script.common_number_of_md_name(md_name_list)
    print(result)
    assert result == True
def original_data_check():
    file = 'palash_4000.tsv'
    result = test_script.test(file)
    print(result)
    assert result == "{('4936586022_F', 'Organism', 'Mus  musculus', '4000'): ['Plural Error', 'Spacing Error'], ('4936586023_F', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586023_A', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586022_C', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586024_C', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586024_E', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586023_E', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586023_B', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586022_E', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586023_C', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586024_D', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586001_C', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586024_A', 'Organism', 'Mus musculus', '4000'): ['Plural Error'], ('4936586022_B', 'Organism', 'Mus musculus', '4000'): ['Plural Error']}"
