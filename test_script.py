import re
from collections import Counter
def rule_for_checking_plural(md_value, error_list):
    # print("md_name - "+ md_name)
    md_value_plural_errors=""
    md_value_plural_check_list = md_value.split(' ')
    # print(md_value_plural_check_list)
    for word in md_value_plural_check_list:
        if word.endswith('s'):
            md_value_plural_errors = "Plural Error"
            error_list.append(md_value_plural_errors)
            break
            print(error_list)
    return error_list

def rule_for_checking_uniqueness(md_name,chipid_dict,chip_id,error_list):
    md_name_uniqueness_error = ""
    if md_name in chipid_dict[chip_id]:
        md_name_uniqueness_error = "Not Unique"
        error_list.append(md_name_uniqueness_error)
    return error_list

def rule_for_checking_white_spaces(md_value,error_list):
    md_value_white_space_errors = ""
    md_value = md_value.rstrip()
    has_leading_white_space = False
    has_middle_white_space = False
    if md_value.strip() != md_value:
        has_leading_white_space = True
        md_value = md_value.strip()

    for i in range(len(md_value) - 1):
        if md_value[i].isspace(): # if the current character is space, then we neet to check the next one.
            if md_value[i+1].isspace(): # if it's still a space, we can tell it's True
                has_middle_white_space = True


    if has_leading_white_space:
        md_value_white_space_errors += "Error at starting"
    if has_middle_white_space:
        md_value_white_space_errors += "Spacing Error"

    if (has_leading_white_space == True) or (has_middle_white_space == True):
        error_list.append(md_value_white_space_errors)
    if has_middle_white_space == True and has_leading_white_space == True:
        error_list.append(md_value_white_space_errors)
    return error_list

def common_number_of_md_name(md_name_list):
    counts = Counter(md_name_list)# it iterate over the list and bring the count of each md_name occurrence such as [ID,ID,Sample, Description] so output will have [ID:2,Sample:1,Description:1] as a key value pair

    if len(set(counts.values()))!=1:
        highest_sample_occurrence = max(counts, key=counts.get)
        highest_count_occurrence=max(counts.values())
        return ("Discrepancy found. Expected list should be as MD NAME : " +str(highest_sample_occurrence)+ " and COUNT : " +str(highest_count_occurrence)+" Error Details are : "+ str(counts))
    else:
        return True


def test(file):
    data_file = open(file)
    chipid_dict={}
    error_dict={}
    count=0
    uniqueness_result = []
    for line in data_file:
        count = count + 1
        error_list = []
        if count >= 2:
            values = line.split('\t')
            chip_type = values[0]
            chip_id = values[1]
            md_name = values[2]
            md_value = values[3]
            ds_id = values[4]
            ds_id = ds_id.rstrip()
            if chip_id not in chipid_dict:
                chipid_dict[chip_id] = {}

            error_list = rule_for_checking_plural(md_value,error_list)
            error_list = rule_for_checking_uniqueness(md_name,chipid_dict,chip_id,error_list)
            error_list = rule_for_checking_white_spaces(md_value,error_list)
            if error_list != []:
                error_dict[(chip_id,md_name,md_value,ds_id)] = error_list
            #error_dict = check_all_errors(check_plural,check_uniqueness,check_whitespaces,error_dict,chip_id,md_name,md_value,md_name_list)
            md_name_list.append(md_name)
            chipid_dict[chip_id][md_name] = md_value

    discrepancy_check = common_number_of_md_name(md_name_list)
    if discrepancy_check ==True:
        print("No Discrepancy")
    else:
        print(discrepancy_check)
    data_file.close()
    print (error_dict)

    return error_dict,discrepancy_check

    #print(error_dict)

#run=test()
